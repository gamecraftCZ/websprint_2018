from django.contrib import admin
from django.conf.urls import url
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns = [
    # index
    url(r'^$', views.index, name="index"),
]

urlpatterns += staticfiles_urlpatterns()