from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test



def admin_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=''):
    ''' Decorator for views that checks that the logged in user is a reception, '''

    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.isAdmin,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator

def visitor_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=''):
    ''' Decorator for views that checks that the logged in user is a visitor, '''

    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.isVisitor,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator



def reception_required(function=None, redirect_field_name=REDIRECT_FIELD_NAME, login_url=''):
    ''' Decorator for views that checks that the logged in user is a reception, '''

    actual_decorator = user_passes_test(
        lambda u: u.is_active and u.isReception,
        login_url=login_url,
        redirect_field_name=redirect_field_name
    )
    if function:
        return actual_decorator(function)
    return actual_decorator