from django.shortcuts import render, redirect
from django.views.generic import View
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout

from .models import BuzeniModel


# Create your views here.
def index(request):
    if not request.user.is_authenticated:
        return redirect("/HotelManager/login")

    if request.user.isVisitor:
        return render(request, 'HotelManager/menu2.html')


    if request.user.isReception:
        users = get_user_model().objects.filter(isVisitor=True).all()
    elif request.user.isAdmin:
        users = get_user_model().objects.all()

    buzeniList = BuzeniModel.objects.all()

    context = {
        "buzeni": buzeniList,
        "users": users
    }
    return render(request, 'HotelManager/adminMenu.html', context)

def masaze(request):
    if not request.user.is_authenticated:
        return redirect("/HotelManager/login")

    context = {

    }
    return render(request, 'HotelManager/masaze.html', context)


class BuzeniView(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return redirect("/HotelManager/login")

        # if request.user.isVisitor:
        return self.get_visitor(request)
        # else:
        #     return self.get_reception(request)



    def post(self, request):
        alarms = BuzeniModel.objects.filter(user=request.user)

        for alarmName in request.POST:
            time = request.POST[alarmName]
            alarmId = alarmName.split("-")[0]

            alarm = alarms.get(id=alarmId)
            if alarm:
                alarm.time = time
                alarm.save()

        context = {
            "buzeni": alarms,
            "success": "Budíky úspěšně uloženy"
        }
        return render(request, 'HotelManager/buzeni.html', context)



    def get_visitor(self, request):
        alarms = BuzeniModel.objects.filter(user=request.user)

        if not alarms:
            alarms = [BuzeniModel(user=request.user, date="20.10", time="8:00"),
                      BuzeniModel(user=request.user, date="21.10", time="8:00"),
                      BuzeniModel(user=request.user, date="22.10", time="8:00"),
                      BuzeniModel(user=request.user, date="23.10", time="8:00"),
                      BuzeniModel(user=request.user, date="24.10", time="8:00"),
                      BuzeniModel(user=request.user, date="25.10", time="8:00"),
                      BuzeniModel(user=request.user, date="26.10", time="8:00")]
            for alarm in alarms:
                alarm.save()

        context = {
            "buzeni": alarms
        }
        return render(request, 'HotelManager/buzeni.html', context)

    def get_reception(self, request):
        return redirect("/HotelManager/")




def snidane(request):
    if not request.user.is_authenticated:
        return redirect("/HotelManager/login")

    day1 = Day()
    day1.date = "1.10. Po"
    day1.foods = ["Tvoje máma", "Moje máma", "Jeho máma"]
    day2 = Day()
    day2.date = "fd"
    day2.foods = ["Tvfdsafdmáma", "Mofewf"]
    context = {
        "snidane": [day1, day2]
    }
    return render(request, 'HotelManager/snidane.html', context)

class Day():
    date = ""
    foods = ""

class Alarm():
    date = ""
    time = ""

class Alarm():
    date = ""
    time = ""

class LoginFormView(View):
    template_name = "HotelManager/login.html"
    redirect_target = "/HotelManager/"

    # return login page
    def get(self, request):
        if request.user.is_authenticated:
            return redirect(self.redirect_target)

        return render(request, self.template_name)

    # try to login
    def post(self, request):
        if request.user.is_authenticated:
            return redirect(self.redirect_target)

        # Normalize data
        username = request.POST["username"]
        password = request.POST["password"]

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return redirect(self.redirect_target)

        return render(request, self.template_name, {"error": "Přihlášení selhalo!"})


def logout_view(request):
    logout(request)
    return redirect("/HotelManager/login")


class AddUser(View):
    template_name = "TemplatesInProgress/addUser.html"
    redirect_target = "/HotelManager/"
    successfulRedirect = "/HotelManager/accountDetails/"

    # return login page
    def get(self, request):
        if (not request.user.is_authenticated) or request.user.isVisitor:
            return redirect(self.redirect_target)

        context = {
            "user": request.user
        }
        return render(request, self.template_name, context)

    # try to login
    def post(self, request):
        if (not request.user.is_authenticated) or request.user.isVisitor:
            return redirect(self.redirect_target)

        # data
        username = request.POST["username"]
        accountType = request.POST["type"]

        if get_user_model().objects.filter(username=username).exists():
            return render(request, self.template_name, {"error": "Uživatel {} již existuje!".format(username)})


        passwordLength = 6
        password = get_user_model().objects.make_random_password(passwordLength)

        isVisitor = False
        isReception = False
        isAdmin = False

        if accountType == "visitor":
            isVisitor = True
        if request.user.isAdmin:
            if accountType == "reception":
                isReception = True
            elif accountType == "admin":
                isAdmin = True


        if not (isVisitor or isReception or isAdmin):
            return render(request, self.template_name, {"error": "Nemáš dostatečná oprávnění pro přidání uživatele!"})

        if isVisitor:
            passwordPlain = password

        user = get_user_model().objects.create_user(username=username,
                                                    password=password,
                                                    passwordPlaintext=passwordPlain,
                                                    isVisitor=isVisitor,
                                                    isReception=isReception,
                                                    isAdmin=isAdmin)
        user.passwordPlaintext = password

        if user:
            return render(request, self.template_name,
                          {"error": "Uživate {} přidán, heslo: {}".format(username, password)})

        return render(request, self.template_name, {"error": "Přidání uživatele selhalo!"})




def getUsers(request):
    if request.user.is_authenticated:
        if request.user.isReception:
            users = get_user_model().objects.filter(isVisitor=True).all()
        elif request.user.isAdmin:
            users = get_user_model().objects.all()
        else:
            return redirect("/HotelManager/")

        context = {
            "users": users
        }
        return render(request, 'HotelManager/usersList.html', context)




