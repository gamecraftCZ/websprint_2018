from django.contrib import admin
from django.conf.urls import url
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns



urlpatterns = [
    # index
    url(r'^$', views.index, name="index"),
    url(r'^masaze', views.masaze, name="masaze"),
    url(r'^snidane', views.snidane, name="snidane"),
    url(r'^buzeni', views.BuzeniView.as_view(), name="buzeni"),

    # /HotelManager/login
    url(r'^login/$', views.LoginFormView.as_view(), name="login"),
    # /HotelManager/logout
    url(r'^logout/$', views.logout_view, name="logout"),

    # /HotelManager/addUser
    url(r'^addUser/$', views.AddUser.as_view(), name="adduser"),
]

urlpatterns += staticfiles_urlpatterns()
